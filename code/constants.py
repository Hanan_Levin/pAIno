import numpy as np, parameters as p

# time indication for midi creation
QUARTER = 32
EIGHTH = 16
HALF = 64

W_K_M = np.array([2.2, 2, 14.3])
up_y = 10 * W_K_M[1]
down_y = -0.5 * W_K_M[1]
back_z = -1 * W_K_M[2]
front_z = W_K_M[2]
base_y = 1.5 * W_K_M[1]
base_z = 3 * W_K_M[0]
x_const = 7  # constant to add to the box in the x axis.

# based on one result, only true for current numbers
# cube for shoulder
x_start = p.joint_pos[1][0]
y_start = p.joint_pos[1][1]
z_start = p.joint_pos[1][2] + W_K_M[2] / 4
cube_length = W_K_M[0] * 4

# an approximated maximum distance for the finger to a far note. used for normalizing the parameters.
white_keys = np.ceil(p.num_keys*7/12)
max_dist = 2 *(white_keys // 2 + x_const) * W_K_M[0]

def update_constants():
    global x_start, y_start, z_start
    x_start = p.joint_pos[1][0]
    y_start = p.joint_pos[1][1]
    z_start = p.joint_pos[1][2] + W_K_M[2] / 4

radius_from_key_goal = 1  # when space is continuous
radius_crit_zone = 0.5    # for the fingertip heuristic
