# parameters for the learning algorithms.
# change all hyper parameters here for ease of use.
CONTINUOUS, DISCRETE = "Continuous", "Discrete"
LJ, OTJ_FIRST, OTJ_FULL, CLOSE_NOTES, FAR_NOTES = "Little Jonathan", "Ode To Joy-First", "Ode To Joy", "CN", "FN"
Q_AGENT, APPQ_AGENT, A_STAR = "Q Agent", "Approximate", "A STAR"
ITER_TIME, MEMORY, EXPANDED, OPERATIONS, ALPHA_GAMMA = "ITER_TIME", "MEMORY", "EXPANDED", "OPERATIONS", "ALPHA_GAMMA"

# for internal use only. do not change
make_space_discrete = None  # if 1, we treat the space as discrete sections, else, the space is treated as continuous
# learning agent types


debug = False
display = True
graph_producing = False
midi_file = "midi_file.mid"
mid_key = 61
num_keys = 13
num_of_fingers = 0

alpha = 0.2
gamma = 0.5
epsilon = 0.1
num_training = 100
num_performances = 1
num_iterations = num_training + num_performances
sound = True  # 1 to play the notes, 0 for silent mode.
display_training = 0

learners_type = [A_STAR]  # Choose Q_AGENT, APPQ_AGENT, A_STAR
pieces = [LJ]  # Choose LJ / OTJ_FIRST / OTJ_FULL / CLOSE_NOTES / FAR_NOTES
bones_list = [2]
discrete_list = [DISCRETE]  # Choose DISCRETE / CONTINUOUS
stats = [ITER_TIME]  # Choose ITER_TIME, MEMORY, EXPANDED, OPERATIONS, ALPHA_GAMMA
alpha_list = [0.2]
gamma_list = [0.5]

joint_pos = [[10, 2, 39], [3, 6.5, 12], [0, 3, 0]]
num_bones = 3  # change number of bones here
learner_type = A_STAR
pivot = joint_pos[0]


def update_params(num_of_bones, a, g):
    global num_bones, joint_pos, pivot, alpha, gamma
    num_bones = num_of_bones
    if num_of_bones == 1:
        joint_pos = [[0, 2, 30], [0, 3, 0], [3, 6.5, 12], [0, 3, 0]]
    if num_of_bones == 2:
        joint_pos = [[10, 2, 39], [3, 6.5, 12], [0, 3, 0]]
    if num_of_bones == 3:
        joint_pos = [[-7, 2, 53], [10, 1.5, 39], [3, 6.5, 12], [0, 3, 0]]
    pivot = joint_pos[0]
    alpha = a
    gamma = g

# measured stating positions
# elbow = [10, 1.5, 39]
# wrist = [3, 6.5, 12]
# shoulder = [-7, 34, 53]
# finger [0, 3, 0] # not measured, just put it above the middle key.
