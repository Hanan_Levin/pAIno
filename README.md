# Project pAIno 


*A team project made for the course 'Introduction to A.I' at the Hebrew University.* 


The project explores methods (Reinforcement learning and Search)  for a 3D model of a hand to play the piano.
A full [report](https://gitlab.com/Hanan_Levin/pAIno/-/blob/master/info/report.pdf) of the project as well as the video of the code in action (shown below) can be viewed under the folder 'info'.
![](info/video.mp4)


## How to run:

### Required Libraries:
1) mido: https://github.com/olemb/mido/
This library is needed to use midi files
2) vpython: http://vpython.org/presentation2018/install.html
This library is needed for the visual display

### Running
1) Install the required libraries
2) set the parameters in the parameters.py file
3) run from the project_runner.py file

### Parameters (a brief explanation parameters.py file)
* 	mid_key: int, midi number of the middle key of the keyboard.
* 	debug: Boolean, if True add prints for debugging
* 	display: Boolean, if True display arm
* 	sound: Boolean, if True make sound
* 	disply_training: Boolean, if True display arm during training
* 	graph_producing: Boolean, if True produce graphs at end of run
* 	num_training: int, number of iterations
* 	learners_type: list, which agents to run
* 	pieces: list, which pieces to play
* 	bones_list: list of ints, how many bones the agents should have
* 	discrete_list: list, what state the space should be - discrete or continuous
* 	stats: list, which stats the graphs should display
* 	alpha_list: list of ints, which alpha values we test
* 	gamma_list: list of ints, which gamma values we test
* 	joint_pos: list of lists, where each list if 3 ints, indicating the x,y,z position of the i-th joint

	

